import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import Budget from "./components/Budget"
import Remaining from "./components/Remaining";
import Spent from "./components/Spent";
import ExpensesList from "./components/ExpensesList";
import AddExpenseForm from "./components/AddExpenseForm";
import { AppProvider } from "./context/AppContext";

const App = () => {
  return (
    <AppProvider>
      <div className="container">
      <h1 className="mt-5 text-center">Personal Budget Tracker</h1>
      <div className="row mt-3">
        <div className="text-center col-sm">
          <Budget />
        </div>
      </div>
      <div className="row mt-3">
        <div className="text-center col-sm">
          <Remaining />
        </div>
        <div className="text-center col-sm">
          <Spent />
        </div>
      </div>
      <h2 className="mt-2 text-center">Expenses</h2>
      <div className="row">
        <div className=" mt-3 col-sm">
          <ExpensesList />
        </div>
      </div>
      <h2 className="mt-4 text-center">Add Expense</h2>
      <div className="mt-2">
        <div className="col-sm">
          <AddExpenseForm />
        </div>
      </div>
    </div>
    </AppProvider>
  )
};


export default App;