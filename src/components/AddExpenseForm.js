import React, {useContext, useState} from 'react';
import {AppContext} from '../context/AppContext';
import {v4 as uuidv4} from 'uuid';

const AddExpenseForm = () => {
    const {dispatch} = useContext(AppContext);

    const [name, setName] = useState('');
	const [cost, setCost] = useState('');

    const onSubmit = (e) => {
        e.preventDefault();

        const expense = {
            id: uuidv4(),
            name: name,
            cost: parseInt(cost),
        };

        dispatch({
            type: 'ADD_EXPENSE',
            payload: expense,
        });
    };

    return (
        <form onSubmit={onSubmit}>
            <div className="row">
                <div className="col-sm">
                    <label for="name"></label>
                    <input required='required' 
                    type="text" 
                    className='form-control' 
                    id="name" 
                    placeholder='Name' 
                    value={name} 
                    onChange={(event) => setName(event.target.value)}></input>

                    <label for="cost"></label>
                    <input required='required' 
                    type="text" 
                    className='form-control' 
                    id="cost" 
                    placeholder='Cost'
                    value={cost} 
                    onChange={(event) => setCost(event.target.value)}></input>

                    <div className="text-center">
                        <button type="submit" className="mt-3 btn btn-primary">
                            Save!
                        </button>
                    </div>

                </div>
            </div>
        </form>
    )
};

export default AddExpenseForm;