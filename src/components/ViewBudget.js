import React from 'react';

const ViewBudget = (props) => {
	return (
		<>
			<span>Budget: ${props.budget}</span>
            <div className="mt-3">
                <button type='button' class='btn btn-primary' onClick={props.handleEditClick}>
                    Edit!
                </button>
            </div>
		</>
	);
};

export default ViewBudget;